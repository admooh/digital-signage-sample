# adMooH's Digital Signage Sample

This repository is a sample android project with of how to configure a webview to run adMooH properly.

## Explaining adMooH

adMooH's ads are provided by adMooH Agent. A software installed along the digital signage software. It will download and provide the ads to the digital signage software.

The digital signage software can access the ads in adMooH Agent by calling the URL http://localhost:9090/html/{duration}, where duration is the timeframe in your schedule, in seconds. The available durations are 10, 15 and 30 seconds.

> Example of a URL: http://localhost:9090/html/15

That URL will return an HTML, with the proper CSS and JS to play the ad.
If the ad is an image, the HTML will contain an `<img>` tag point to the ad.
If the ad is a video, the HTML will contain an `<video>` tag point to the ad.

The video will have the `autoplay` attribute, so it can play without user interaction.

After the ad has played, a JavaScript in the page will confirm the print once it has played.

## Why we made this project

Because more than one Digital Signage software had problems working with adMooH, specially playing videos, we decided to make this project.

Here we have a fully working app that will open a WebView to adMooH's URLs and play the ad as it should.

## TL;DR

If you understand your way around Android and the WebView

* Enable hardware acceleration in your Activity
* In the settings of your WebView
  * Enable JavaScript
  * Enable the WebView to play videos without the user interaction

## Running the project

You can open it with Android Studio.

The video playback may not work with the emulator.

## Points of interest

### AndroidManifest.xml

In the AndroidManifest.xml file, where the MainActivity is defined, you'll see I've enabled the hardware acelleration. It is required to run videos in the WebView.
You don't have to do this in the MainActivity, just in the activity that will run adMooH's template (http://localhost:9090/html/{duration})

```xml
<activity ... android:hardwareAccelerated="true" ... >
```

### MainActivity.java

I've setup a method that do the configuration of the **WebView**. The method is called `configureWebView`.

In there, the most important parts are _enable JavaScript_ and _enable media playback without user gesture_.

#### Media playback without user gesture

This is required so that any video ad can play without user interaction. This is required because there is no user to interact with a digital signage software all the time.

To enable it use

```java
WebSettings settings = webView.getSettings();
settings.setJavaScriptEnabled(true);
```

#### Enable javascript

Javascript is required so that the template can confirm that the item played. This is part of adMooH's flow and is mandatory.

To enable it use

```java
WebSettings settings = webView.getSettings();
settings.setMediaPlaybackRequiresUserGesture(false);
```
