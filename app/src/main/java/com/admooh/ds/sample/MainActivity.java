package com.admooh.ds.sample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        webView = findViewById(R.id.webview);

        configureWebView(webView);
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadDuration(15);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        int[] durations = new int[] {10, 15, 30};

        MenuItem refreshItem = menu.add("Refresh");
        refreshItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        refreshItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                MainActivity.this.webView.reload();
                return true;
            }
        });

        for(int duration : durations){
            addMenuItemForDuration(menu, duration);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
       Toast.makeText(this, "onOptionsItemSelected", Toast.LENGTH_LONG).show();

       return super.onOptionsItemSelected(item);
    }

    /**
     * This method configures the WebView with whats required for it to run properly
     */
    private void configureWebView(WebView webView) {
        webView.setWebChromeClient(new ConsoleLogChromeClient(this));

        WebSettings settings = webView.getSettings();

        // This method enables the video to be playable without user interaction
        settings.setMediaPlaybackRequiresUserGesture(false);

        // This is required for confirming the print, otherwise the print won't be confirmed
        settings.setJavaScriptEnabled(true);

        // These two are just to make sure the network request can occur without problems
        // Maybe we don't need them, but I don't trust android about these things -.-
        settings.setBlockNetworkImage(false);
        settings.setBlockNetworkLoads(false);
    }

    private void loadDuration(int duration){
        webView.loadUrl("http://localhost:9090/html/" + duration);
    }

    private void addMenuItemForDuration(Menu menu, final int duration) {
        MenuItem item = menu.add(duration + " seconds");
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);


        item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                loadDuration(duration);
                return true;
            }
        });
    }
}
