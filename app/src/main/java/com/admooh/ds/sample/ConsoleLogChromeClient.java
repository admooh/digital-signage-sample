package com.admooh.ds.sample;

import android.content.Context;
import android.util.Log;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by vinicius on 3/22/2018.
 */

public class ConsoleLogChromeClient extends WebChromeClient {

    private File externalFilesDir;

    public ConsoleLogChromeClient(Context context){
        externalFilesDir = context.getExternalFilesDir(null);
    }

    @Override
    public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
        try {
            File file = new File(externalFilesDir, "webview_logs.txt");

            if(!file.exists())
                file.createNewFile();

            Date currentTime = Calendar.getInstance().getTime();

            FileOutputStream outputStreamWriter = new FileOutputStream (file, true);

            String message = currentTime.toString() + " - " + consoleMessage.messageLevel().name() + ": " + consoleMessage.message() + "\n";

            outputStreamWriter.write(message.getBytes());
            outputStreamWriter.flush();

        } catch(java.lang.Exception ex) {
            Log.e("WebView_log", "Error writing JS log to file.", ex);
        }
        return super.onConsoleMessage(consoleMessage);
    }
}
